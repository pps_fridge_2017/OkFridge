class Controller:
    def __init__(self):
        self._method_map = dict()

    def add_method(self, f, name):
        self._method_map[name] = f

    def add_methods(self, methods):
        self._method_map.update(methods)

    def execute(self, method, *args, **kwargs):
        return self._method_map[method](*args, **kwargs)

    @property
    def methods(self):
        return self._method_map


controller = Controller()
