import sqlite3
import numpy as np

from storing_module.product import Product
from storing_module.storing_module import StoringModule
from storing_module.table import Table
from controller.controller import controller


class StoringModuleController(StoringModule):
    def __init__(self, main_dbh='main.db', geography_file='field.txt'):
        self.main_dbh = sqlite3.connect(main_dbh)
        self.geography_file = geography_file

        table_descriptions = {
            (
                'products',
                [
                    ('id', 'INTEGER'),
                    ('category', 'TEXT'),
                    ('state', 'INTEGER'),
                    ('name', 'TEXT'),
                ],
                'PRIMARY KEY (id) ON CONFLICT REPLACE'
            )
        }

        self.tables = {td[0]: Table(self.main_dbh, *td) for td in table_descriptions}

        controller.add_methods({
            'model_write_df': self.write_df,
            'model_read_df': self.read_df,
            'model_remove_df': self.remove_df,
            'model_get_geography': self.__get_geography
        })

    def write_df(self, table_name, df):
        self.tables[table_name].write_df(df)

    def read_df(self, table_name, query=None, **kwargs):
        self.tables[table_name].read_df(query, **kwargs)

    def remove_df(self, table_name, df):
        self.tables[table_name].remove_df(df)

    def get_product_by_name(self, name):
        df = controller.execute('model_read_df', 'products')
        df = df[df.name == name]
        if df.empty:
            return None
        else:
            return Product(*df.values()[0])

    def __get_geography(self):
        with open(self.geography_file, 'r') as file:
            return np.array([list(line) for line in file.read().splitlines()])
