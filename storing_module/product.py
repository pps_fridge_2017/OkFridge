import pandas as pd


def product_df(products):
    return pd.DataFrame([[p.id, p.category, p.state, p.name] for p in products],
                        columns=['id', 'category', 'state', 'name'])


class Product:
    def __init__(self, id=1, category='water', state=1, name='miratorg'):
        self.id = id
        self.category = category
        self.state = state
        self.name = name
