import pandas as pd


class Table:
    def __init__(self, dbh, table, schema, modifier="", pre_dump=None, post_load=None):
        self.schema = schema
        self.table = table
        self.dbh = dbh
        self.pre_dump = pre_dump or (lambda x: x)
        self.post_load = post_load or (lambda x: x)

        self.__create_table(modifier)

    def __create_table(self, modifier):
        columns = [" ".join(['"{}"'.format(name), data_type]) for name, data_type in self.schema]

        if modifier:
            modifier = ", " + modifier

        self.dbh.cursor().execute(
            'CREATE TABLE IF NOT EXISTS "{table}" ({columns}{modifier})'.format(
                table=self.table,
                columns=",".join(columns),
                modifier=modifier))

    def write_df(self, df):
        self.pre_dump(df).to_sql(self.table, self.dbh, if_exists='append', index=False)

    def read_df(self, query=None, **kwargs):
        if query is None:
            query = 'SELECT * FROM "{table}"'
        return self.post_load(pd.read_sql(query.format(table=self.table), self.dbh, **kwargs))

    def remove_df(self, df):
        self.dbh.cursor().executemany(
            'DELETE FROM {table} WHERE {conds}'.format(
                table=self.table,
                conds=' AND '.join('{col} = ?'.format(col=col) for col in df.columns)
            ),
            df.values()
        )

        self.dbh.commit()
