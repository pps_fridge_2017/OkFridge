import sys

from voice_module.voice_module_controller import VoiceModuleController
from detection_module.detection_module import DetectionModule
from storing_module.product import Product

DOOR_OPENED = "door_opened"
DOOR_CLOSED = "door_closed"
PUT_IN_FRIDGE = "put_in_fridge"
TAKE_FROM_FRIDGE = "take_from_fridge"
SAY = "say"


if __name__ == '__main__':
    voice_module = VoiceModuleController()
    detection_module = DetectionModule()
    product_detector = detection_module.product_detector
    print("fridge is running")
    for line in sys.stdin:
        line = line[:-1]
        if line.find(DOOR_OPENED) != -1:
            detection_module.door_opened()
        if line.find(DOOR_CLOSED) != -1:
            detection_module.door_closed()
        if line.find(PUT_IN_FRIDGE) != -1:
            product = line.split(' ')[-1]
            product_detector.simulate_detect(True, Product(category=product))
        if line.find(TAKE_FROM_FRIDGE) != -1:
            product = line.split(' ')[-1]
            product_detector.simulate_detect(False, Product(category=product))
        if line.find(SAY) != -1:
            splitted_line = line.split()
            data = ' '.join(splitted_line[1:])
            voice_module._microphone.receive_data(data)
