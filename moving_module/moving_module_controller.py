import pandas as pd
from moving_module.moving_module import *
from storing_module.product import *
from controller.controller import *
from resources import *


class MovingTrunc:
    def __init__(self, field_path='field.txt'):
        self.field = []
        for line in field_path.readlines():
            self.field.append(list(line))
        for i in range(len(self.field)):
            for j in range(len(self.field[0])):
                if self.field[i][j] == 'F':
                    self.fridge = (i, j)
                if self.field[i][j] == 'O':
                    self.owner = (i, j)

    def __print_field(self):
        for i in range(len(self.field)):
            for j in range(len(self.field[0])):
                print(self.field[i][j])
            print

    def move_to_owner(self):
        cur_fridge = self.fridge
        self.__print_field()
        d = [(self.owner[0] - self.fridge[0]), (self.owner[1] - self.fridge[1])]
        for i in range(2):
            if abs(d[i]) > 0:
                step = d[i] / abs(d[i])
            while abs(d[i]) > 0:
                self.field[cur_fridge[0]][cur_fridge[1]] = '.'
                cur_fridge[i] += step
                self.field[cur_fridge[0]][cur_fridge[1]] = 'F'
                self.__print_field()
                d = [(self.owner[0] - cur_fridge[0]), (self.owner[1] - cur_fridge[1])]


class MovingModuleController(MovingModule):

    def __get_product_from_fridge(self, product):
        try:
            controller.execute('model_remove_df', 'products', pd.DataFrame({'id': product.id,
                                                                            'category': product.category,
                                                                            'state': product.state,
                                                                            'name': product.name}))
            trunc = MovingTrunc()
            trunc.move_to_owner()
            print('product succesfully delivered')
        except Exception:
            print('there is no such product in fridge')

    def deliver(self, product):
        self.__get_product_from_fridge(product)
