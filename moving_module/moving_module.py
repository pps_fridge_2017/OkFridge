class MovingModule:
    def deliver(self, product):
        raise NotImplementedError('users must define deliver to use this base class')
