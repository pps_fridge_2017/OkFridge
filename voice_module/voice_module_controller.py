import threading

from controller.controller import controller
from voice_module.dynamic import Dynamic
from voice_module.microphone import Microphone
from voice_module.voice_module import VoiceModule


class VoiceModuleController(VoiceModule):
    def __init__(self):
        print("start initializing voice module")
        controller.add_methods({"set_voice_massage": self.set_voice_message})
        self._dynamic = Dynamic()
        self._microphone = Microphone()
        print("start listening thread module")
        listening_thread = threading.Thread(target=self.__listen_for_voice_message, args=())
        listening_thread.daemon = True
        listening_thread.start()
        print("voice module is initialized")

    def set_voice_message(self, message_text):
        self._dynamic.play_voice_message(message_text)

    def __listen_for_voice_message(self):
        while True:
            data = self._microphone.listen()
            while data == "":
                data = self._microphone.listen()
            self.__process_data(data)

    def __process_data(self, data):
        print(data)

