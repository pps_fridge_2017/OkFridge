class Microphone:
    def __init__(self):
        self.__data_received = False
        self.__data = ""

    # mock for real moment when data is received
    def receive_data(self, data):
        self.__data = data
        self.__data_received = True

    def listen(self):
        if self.__data_received:
            data_to_send = self.__data
            self.__data = ""
            return data_to_send
        else:
            return ""
