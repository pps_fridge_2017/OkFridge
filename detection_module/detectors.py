from detection_module.detector import Detector


class ProductDetector(Detector):
    def simulate_detect(self, put, product):
        if put:
            self.detection_module.product_is_put(product)
        else:
            self.detection_module.product_is_taken(product)
