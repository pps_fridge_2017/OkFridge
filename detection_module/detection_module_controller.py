from controller.controller import controller
from detection_module.detection_module import DetectionModule
from detection_module.detectors import ProductDetector
from storing_module.product import product_df


class DetectionModuleController(DetectionModule):
    def __init__(self):
        self._product_detector = ProductDetector(self)

    def __product_is_put(self, product):
        print('{} is put in the fridge'.format(product))
        controller.execute('model_write_df', 'products', product_df([product]))

    def __product_is_taken(self, product):
        print('{} is taken from the fridge'.format(product))
        controller.execute('model_remove_df', 'product', product_df([product]))